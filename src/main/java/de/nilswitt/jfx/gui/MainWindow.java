package de.nilswitt.jfx.gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Main JFX Window
 */
public class MainWindow extends Application {
    @Override
    public void start(Stage stage) {
        BorderPane bp = new BorderPane();
        bp.setCenter(new Text("Hello World!"));
        Scene scene = new Scene(bp, 200, 200);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
