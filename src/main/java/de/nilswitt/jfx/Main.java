package de.nilswitt.jfx;

import de.nilswitt.jfx.gui.MainWindow;

public class Main {

    /**
     * Main entrypoint
     * @param args
     */
    public static void main(String[] args) {
        MainWindow.main(args);
    }
}
