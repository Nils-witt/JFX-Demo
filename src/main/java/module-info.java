module de.nilswitt.jfx {

    requires javafx.controls;
    requires java.base;

    exports de.nilswitt.jfx;
    exports de.nilswitt.jfx.gui;
}
